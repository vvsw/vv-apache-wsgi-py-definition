#!/bin/python

VERBOSE=True

import os,sys
path = os.getcwd()
sys.path.insert(0,'/var/www/html')  # hard coded like 


def _debug(s):
        if VERBOSE:
                print("[DEBUG]: " + str(s))

from urllib.parse import parse_qs


try:
        from handler import handler
        assert(handler)
        _debug("handler.py loaded successfully.")
except Exception as e:
        raise ValueError("[ERROR]: handler.py not imported successfully.i\n[ERROR]: path=" + str(path) + " \n[ERROR]: error=" + str(e) + "\n")


def application (environ, start_response):

        html = """
                <html>
                <body>
                                <img src="https://vvsw.gitlab.io/vv-http-sandbox/images/favicon.png" height="14px"></img><br>
                        <br>
                        handler.py
                    <p>
                        <b>vv_handler_input:</b> %(arguments)s<br>
                        <b>vv_handler_output:</b> %(vv_handler_output)s
                    </p>
                </body>
                        """

        # Returns a dictionary in which the values are lists
        d = parse_qs(environ['QUERY_STRING'])

        response_body = html % { # Fill the above html template in
            'arguments': d or '<user did not specify arguments>',
            'vv_handler_output': handler(d)
        }

        

        status = '200 OK'

        # Now content type is text/html
        response_headers = [
            ('Content-Type', 'text/html'),
            ('Content-Length', str(len(response_body)))
        ]

        start_response(status, response_headers)
        response_body = bytes(response_body, 'utf-8')
        return [response_body]


def test():
        env={
                'QUERY_STRING': '&data=test123&data2=test432'
        }
        application(env, lambda x,y: 'x_return_str' )

def main():
        test()

if __name__ == "__main__":
# execute only if run as a script
        main()